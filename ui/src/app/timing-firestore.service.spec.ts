import { TestBed } from '@angular/core/testing';

import { TimingFirestoreService } from './timing-firestore.service';

describe('TimingFirestoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimingFirestoreService = TestBed.get(TimingFirestoreService);
    expect(service).toBeTruthy();
  });
});
