import { TestBed } from '@angular/core/testing';

import { TimingRealtimeDbService } from './timing-realtime-db.service';

describe('TimingRealtimeDbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimingRealtimeDbService = TestBed.get(TimingRealtimeDbService);
    expect(service).toBeTruthy();
  });
});
