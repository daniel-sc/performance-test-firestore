import {Component} from '@angular/core';
import {interval, Observable} from 'rxjs';
import {map, take, tap} from 'rxjs/operators';
import {Data} from './data';
import {TimingFirestoreService} from './timing-firestore.service';
import {TimingWebsocketService} from './timing-websocket.service';
import {TimingService} from './timing.service';
import {TimingRealtimeDbService} from './timing-realtime-db.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private _clientId: string;

  public readonly services: TimingService[];

  constructor(timingFirestore: TimingFirestoreService,
              timingRealtimeDb: TimingRealtimeDbService,
              timingWs: TimingWebsocketService) {
    this.services = [timingFirestore, timingRealtimeDb, timingWs];
    // set clientId only after initial updates are processed:
    setTimeout(() => this.clientId = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.charAt(Math.random() * 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.length), 5000);
  }

  start() {
    this.services.forEach(s => s.start({clientId: this.clientId, start: Date.now(), responses: []}));
  }

  start10() {
    interval(1000).pipe(take(10)).subscribe(() => this.start());
  }

  getRoundtripTimes(test: Data) {
    return test.responses
      .filter(r => r.received)
      .map(r => r.received - test.start);
  }

  set clientId(clientId: string) {
    this._clientId = clientId;
    this.services.forEach(s => s.clientId = clientId);
  }

  get clientId() {
    return this._clientId;
  }

  getRoundtripTimesAsResult(test: Observable<Data[]>) {
    return test.pipe(
      map(testResult => testResult.map(t => ({
        name: t.start,
        series: t.responses
          .filter(r => r.received)
          .map(r => ({name: r.clientId, value: r.received - t.start}))
      }))),
      // tap(x => console.log('result: ', x))
    );
  }
}
