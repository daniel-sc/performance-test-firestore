import {Observable} from 'rxjs';
import {Data} from './data';

export interface TimingService {

  readonly name: string;
  clientId: string;
  readonly testResults: Observable<Data[]>;

  start(timing: Data): void;

  clear(): void;
}
