export interface Data {
  clientId: string;
  start: number;
  responses: Array<{
    clientId: string;
    sent: number;
    received: number;
  }>;
}
