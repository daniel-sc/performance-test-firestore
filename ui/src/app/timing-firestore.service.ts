import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {filter, flatMap, map, shareReplay, take} from 'rxjs/operators';
import * as firebase from 'firebase';
import {Data} from './data';
import {Observable} from 'rxjs';
import {TimingService} from './timing.service';

@Injectable({
  providedIn: 'root'
})
export class TimingFirestoreService implements TimingService {

  readonly name = 'Firestore';
  clientId = null;

  readonly testResults: Observable<Data[]>;

  private pendingUpdates = new Set(); // start + response.clientId

  private collectionName = 'latency-tests';

  constructor(private db: AngularFirestore) {
    this.initResponses(db);
    this.initCollectResponses(db);
    this.testResults = db.collection(this.collectionName).valueChanges().pipe(
      map(s => s as Data[]),
      map(s => s.sort((a, b) => a.start > b.start ? 1 : -1)),
      shareReplay(1)
    );
  }

  start(timing: Data) {
    this.db.collection(this.collectionName).add(timing);
  }

  clear(): void {
    this.db.collection(this.collectionName).snapshotChanges().pipe(
      take(1),
      flatMap(changes => changes),
      flatMap(change => change.payload.doc.ref.delete())
    ).subscribe();
  }

  private initResponses(db: AngularFirestore) {
    db.collection(this.collectionName).stateChanges(['added'])
      .pipe(
        flatMap(changes => changes),
        filter(change => (change.payload.doc.data() as Data).clientId !== this.clientId)
      )
      .subscribe(changeAction => {
        if (!this.clientId) {
          return;
        }
        const newDoc = changeAction.payload.doc.data() as Data;
        // console.log('new document: ', newDoc);
        if (newDoc.responses && !newDoc.responses.some(r => r.clientId === this.clientId)) {
          changeAction.payload.doc.ref.update('responses', firebase.firestore.FieldValue.arrayUnion({
            clientId: this.clientId,
            sent: Date.now(),
            received: null
          }));
        }
      });
  }

  private initCollectResponses(db: AngularFirestore) {
    db.collection(this.collectionName).stateChanges(['modified'])
      .pipe(
        flatMap(changes => changes),
        filter(change => (change.payload.doc.data() as Data).clientId === this.clientId)
      )
      .subscribe(changeAction => {
        if (!this.clientId) {
          return;
        }
        const newDoc = changeAction.payload.doc.data() as Data;
        // console.log('modified document: ', newDoc);
        newDoc.responses.filter(r => !r.received && !this.pendingUpdates.has(newDoc.start + r.clientId)).forEach(r => {
          const newResponse = Object.assign({}, r, {received: Date.now()});
          this.pendingUpdates.add(newDoc.start + r.clientId);
          changeAction.payload.doc.ref.update('responses', firebase.firestore.FieldValue.arrayRemove(r))
            .then(() => changeAction.payload.doc.ref.update('responses', firebase.firestore.FieldValue.arrayUnion(newResponse)))
            .then(() => this.pendingUpdates.delete(newDoc.start + r.clientId));
        });
      });
  }

}
