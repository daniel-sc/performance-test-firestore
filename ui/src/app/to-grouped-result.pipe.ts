import {Pipe, PipeTransform} from '@angular/core';
import {Data} from './data';

@Pipe({
  name: 'toGroupedResult',
  pure: true
})
export class ToGroupedResultPipe implements PipeTransform {

  transform(value?: Data[], args?: any): any {
    return value ? value.map((t, index) => ({
      name: index,
      series: t.responses
        .filter(r => r.received)
        .map(r => ({name: r.clientId, value: r.received - t.start}))
    })) : [];
  }

}
