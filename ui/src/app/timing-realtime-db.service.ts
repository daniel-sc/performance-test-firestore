import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {TimingService} from './timing.service';
import {Data} from './data';
import {Observable} from 'rxjs';
import {filter, map, shareReplay, tap} from 'rxjs/operators';

export interface DataRealtime {
  clientId: string;
  start: number;
  responses: {
    [clientId: string]: {
      sent: number;
      received: number;
    }
  };
}


@Injectable({
  providedIn: 'root'
})
export class TimingRealtimeDbService implements TimingService {

  constructor(private db: AngularFireDatabase) {
    this.initResponses();
    this.initCollectResponses();
    this.testResults = db.list(this.collectionName).valueChanges().pipe(
      // tap(x => console.log('value change: ', x)),
      map(s => s as DataRealtime[]),
      map(s => s.map(d => TimingRealtimeDbService.dataRealtime2data(d))),
      map(s => s.sort((a, b) => a.start > b.start ? 1 : -1)),
      shareReplay(1)
    );
  }

  clientId: string;
  readonly name = 'Realtime Database';
  readonly testResults: Observable<Data[]>;

  private pendingUpdates = new Set(); // start + response.clientId
  private collectionName = '/latency-tests';

  private static dataRealtime2data(newDoc: DataRealtime): Data {
    return Object.assign({}, newDoc, {
      responses: !newDoc.responses ? [] : Object.entries(newDoc.responses).map(([key, val]) => ({
        clientId: key,
        ...val
      }))
    });
  }

  clear(): void {
    this.db.list(this.collectionName).remove();
  }

  start(timing: Data): void {
    this.db.list(this.collectionName).push(timing);
  }


  private initResponses() {
    this.db.list(this.collectionName).stateChanges(['child_added'])
      .pipe(
        filter(change => (change.payload.val() as Data).clientId !== this.clientId)
      )
      .subscribe(changeAction => {
        if (!this.clientId) {
          return;
        }
        const newDoc = changeAction.payload.val() as DataRealtime;
        if (!newDoc.responses || !newDoc.responses[this.clientId] && !this.pendingUpdates.has(newDoc.start + this.clientId)) {
          this.pendingUpdates.add(newDoc.start + this.clientId);
          this.db.object(this.collectionName + '/' + changeAction.key + '/responses/' + this.clientId).set({
            sent: Date.now(),
            received: null
          }).then(x => console.log('created new reponse: ', x))
            .then(() => this.pendingUpdates.delete(newDoc.start + this.clientId));
        }
      });
  }

  private initCollectResponses() {
    this.db.list(this.collectionName).stateChanges(['child_changed'])
      .pipe(
        filter(change => (change.payload.val() as Data).clientId === this.clientId)
      )
      .subscribe(changeAction => {
        if (!this.clientId) {
          return;
        }
        const received = Date.now();
        const newDoc = changeAction.payload.val() as DataRealtime;
        const newDoc2 = TimingRealtimeDbService.dataRealtime2data(newDoc);
        newDoc2.responses.filter(r => !r.received && !this.pendingUpdates.has(newDoc.start + r.clientId)).forEach(r => {
          const newResponse = Object.assign({}, r, {received: Date.now()});
          this.pendingUpdates.add(newDoc.start + r.clientId);
          this.db.object(`${this.collectionName}/${changeAction.key}/responses/${r.clientId}/received`).set(received)
            .then(() => this.pendingUpdates.delete(newDoc.start + r.clientId))
            .then(() => console.log('updated doc for received time'));
        });
      });
  }
}
