import { TestBed } from '@angular/core/testing';

import { TimingWebsocketService } from './timing-websocket.service';

describe('TimingWebsocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimingWebsocketService = TestBed.get(TimingWebsocketService);
    expect(service).toBeTruthy();
  });
});
