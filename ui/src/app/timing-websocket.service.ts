import {EventEmitter, Injectable} from '@angular/core';
import {TimingService} from './timing.service';
import {EMPTY, Observable} from 'rxjs';
import {Data} from './data';
import {WebSocketSubject} from 'rxjs/webSocket';
import {environment} from '../environments/environment';
import {catchError, map, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TimingWebsocketService implements TimingService {

  readonly name = 'Websocket';
  clientId: string;
  readonly testResults: Observable<Data[]>;

  private readonly ws = new WebSocketSubject(environment.wsEndpoint);
  private testsWs: Data[] = [];
  private readonly newResult = new EventEmitter<Data>();

  constructor() {

    this.testResults = this.newResult.pipe(
      map(r => {
        if (r === null) {
          this.testsWs = [];
        } else {
          const existing = this.testsWs.find(d => d.start === r.start);
          if (existing) {
            existing.responses.push(r.responses[0]);
          } else {
            this.testsWs.push(r);
          }
        }
        return [...this.testsWs];
      }),
      shareReplay(1)
    );

    this.ws.pipe(
      catchError(e => {
        console.error('ws failed', e);
        return EMPTY;
      })
    )
      .subscribe(x => {
        const data = x as Data;
        // console.log('got: ', x);
        if (data.clientId !== this.clientId && data.responses.length === 0) {
          data.responses.push({clientId: this.clientId, sent: Date.now(), received: null});
          this.ws.next(data);
        }
        if (data.clientId === this.clientId && data.responses.length === 1) {
          data.responses[0].received = Date.now();
          this.newResult.emit(data);
        }
      });
  }

  clear(): void {
    this.newResult.emit(null);
  }

  start(timing: Data): void {
    this.ws.next(timing);
  }
}
